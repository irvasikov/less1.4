FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

#RUN wget http://downloads.sourceforge.net/pcre/pcre-8.12.tar.bz2 && tar -xjf pcre-8.12.tar.bz2 && cd
#RUN wget http://zlib.net/zlib-1.2.8.tar.gz && tar -zxvf zlib-1.2.8.tar.gz && cd
RUN wget http://nginx.org/download/nginx-1.20.2.tar.gz && tar xvfz nginx-1.20.2.tar.gz && cd nginx-1.20.2 && ./configure --prefix=/usr/local --with-http_ssl_module && make && make install


FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]


